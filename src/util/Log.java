package android.util;

public class Log {
   private static boolean DEBUG = true;

   private static void buildMessage(String tag, String message) {
      String msg = "[" + tag + "]" + message;
      System.out.println(msg);
   }

   public static void e(String tag, String messag) {
      if (DEBUG) {
         buildMessage(tag, messag);
      }

   }

   public static void e(String tag, String messag, Throwable throwable) {
      if (DEBUG) {
         buildMessage(tag, messag);
      }

   }

   public static void d(String tag, String messag) {
      if (DEBUG) {
         buildMessage(tag, messag);
      }

   }

   public static void w(String tag, String messag) {
      if (DEBUG) {
         buildMessage(tag, messag);
      }

   }

   public static void v(String tag, String messag) {
      if (DEBUG) {
         buildMessage(tag, messag);
      }

   }

   public static void i(String tag, String messag) {
      if (DEBUG) {
         buildMessage(tag, messag);
      }

   }
}
