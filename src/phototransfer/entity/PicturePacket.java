package phototransfer.entity;

public class PicturePacket {
   public static final int MAX_BLOCK_SIZE = 1024;
   private boolean endFlag;
   private boolean startFlg;
   private int size;
   private int num;
   private byte[] buff;

   public boolean isEndFlag() {
      return this.endFlag;
   }

   public void setEndFlag(boolean endFlag) {
      this.endFlag = endFlag;
   }

   public boolean isStartFlg() {
      return this.startFlg;
   }

   public void setStartFlg(boolean startFlg) {
      this.startFlg = startFlg;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int size) {
      this.size = size;
   }

   public byte[] getBuff() {
      return this.buff;
   }

   public void setBuff(byte[] buff) {
      this.buff = buff;
   }

   public int getNum() {
      return this.num;
   }

   public void setNum(int num) {
      this.num = num;
   }

   public static enum BitStructure {
      END_FLAG(1),
      START_FLAG(1),
      SIZE(4),
      NUM(4),
      BUFF(1024);

      private int mBitCount;

      public int getBitCount() {
         return this.mBitCount;
      }

      private BitStructure(int bitCount) {
         this.mBitCount = bitCount;
      }

      public static int getOffset(PicturePacket.BitStructure structure) {
         int len = 0;
         PicturePacket.BitStructure[] var2 = values();
         int var3 = var2.length;

         for(int var4 = 0; var4 < var3; ++var4) {
            PicturePacket.BitStructure s = var2[var4];
            if (s.equals(structure)) {
               break;
            }

            len += s.mBitCount;
         }

         return len;
      }

      public static int getBufferLen() {
         int len = 0;
         PicturePacket.BitStructure[] var1 = values();
         int var2 = var1.length;

         for(int var3 = 0; var3 < var2; ++var3) {
            PicturePacket.BitStructure s = var1[var3];
            len += s.mBitCount;
         }

         return len;
      }
   }
}
