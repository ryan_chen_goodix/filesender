package phototransfer.utils;

public class ArrayUtils {
   public static int byteArr2Int(byte[] bytes, int start) {
      return bytes[0 + start] & 255 | (bytes[1 + start] & 255) << 8 | (bytes[2 + start] & 255) << 16 | (bytes[3 + start] & 255) << 24;
   }

   public static byte[] int2ByteArr(int value) {
      return new byte[]{(byte)(value & 255), (byte)(value >> 8 & 255), (byte)(value >> 16 & 255), (byte)(value >> 24 & 255)};
   }
}
