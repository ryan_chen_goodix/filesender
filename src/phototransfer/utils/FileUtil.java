package phototransfer.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

public class FileUtil {
   private static final String TAG = "FileUtil";

   public static void copyFile(String parentDir, String fileName, File originFile, FileUtil.CopyListener copyListener) {
      try {
         FileInputStream fileInputStream = new FileInputStream(originFile);
         copyFile(parentDir, fileName, fileInputStream, originFile.length(), copyListener);
      } catch (FileNotFoundException var5) {
         var5.printStackTrace();
      }

   }

   public static void copyFile(String parentDir, String fileName, InputStream inputStream, long totalLenth, FileUtil.CopyListener copyListener) {
      try {
         copyListener.startCopy();
         File newFile = new File(parentDir + File.separator + fileName);
         FileOutputStream fileOutputStream = new FileOutputStream(newFile);
         byte[] data = new byte[2048];
         int len = false;
         long currentLenght = 0L;

         int len;
         while((len = inputStream.read(data)) != -1) {
            fileOutputStream.write(data, 0, len);
            currentLenght += (long)len;
            copyListener.progress((int)(currentLenght * 100L / totalLenth));
         }

         copyListener.finish(newFile);
      } catch (FileNotFoundException var12) {
         var12.printStackTrace();
      } catch (IOException var13) {
         var13.printStackTrace();
      }

   }

   public static boolean deleteDir(String delpath) {
      File file = new File(delpath);
      if (!file.isDirectory()) {
         file.delete();
      } else if (file.isDirectory()) {
         String[] filelist = file.list();

         for(int i = 0; i < filelist.length; ++i) {
            File delfile = new File(delpath + "\\" + filelist[i]);
            if (!delfile.isDirectory()) {
               delfile.delete();
               System.out.println(delfile.getAbsolutePath() + "删除文件成功");
            } else if (delfile.isDirectory()) {
               deleteDir(delpath + "\\" + filelist[i]);
            }
         }

         System.out.println(file.getAbsolutePath() + "删除成功");
         file.delete();
      }

      return true;
   }

   public static boolean createFile(String path, String fileName) {
      File file = new File(path + File.separator + fileName);
      if (file.exists()) {
         Logger.w("FileUtil", "新建文件失败：file.exist()=" + file.exists());
         return false;
      } else {
         try {
            boolean isCreated = file.createNewFile();
            return isCreated;
         } catch (IOException var4) {
            var4.printStackTrace();
            return false;
         }
      }
   }

   public static boolean deleteFile(String path, String fileName) {
      File file = new File(path + File.separator + fileName);
      if (file.exists()) {
         boolean isDeleted = file.delete();
         return isDeleted;
      } else {
         return false;
      }
   }

   public static String getFileSuffix(String fileName) {
      int index = fileName.lastIndexOf(".");
      String suffix = fileName.substring(index + 1, fileName.length());
      return suffix;
   }

   public static boolean renameTo(String oldPath, String newPath) {
      if (oldPath.equals(newPath)) {
         Logger.w("FileUtil", "文件重命名失败：新旧文件名绝对路径相同！");
         return false;
      } else {
         File oldFile = new File(oldPath);
         File newFile = new File(newPath);
         boolean isSuccess = oldFile.renameTo(newFile);
         Logger.w("FileUtil", "文件重命名是否成功：" + isSuccess);
         return isSuccess;
      }
   }

   public static boolean renameTo(File oldFile, File newFile) {
      if (oldFile.equals(newFile)) {
         Logger.w("FileUtil", "文件重命名失败：旧文件对象和新文件对象相同！");
         return false;
      } else {
         boolean isSuccess = oldFile.renameTo(newFile);
         Logger.w("FileUtil", "文件重命名是否成功：" + isSuccess);
         return isSuccess;
      }
   }

   public static boolean renameTo(File oldFile, String newName) {
      File newFile = new File(oldFile.getParentFile() + File.separator + newName);
      boolean flag = oldFile.renameTo(newFile);
      return flag;
   }

   public static String formatSize(long size) {
      DecimalFormat df = new DecimalFormat("####.00");
      if (size < 1024L) {
         return size + "Byte";
      } else {
         float gSize;
         if (size < 1048576L) {
            gSize = (float)size / 1024.0F;
            return df.format((double)gSize) + "KB";
         } else if (size < 1073741824L) {
            gSize = (float)size / 1024.0F / 1024.0F;
            return df.format((double)gSize) + "MB";
         } else if (size < 1099511627776L) {
            gSize = (float)size / 1024.0F / 1024.0F / 1024.0F;
            return df.format((double)gSize) + "GB";
         } else {
            return "size: error";
         }
      }
   }

   public static File[] getFileList(String path) {
      File file = new File(path);
      if (file.isDirectory()) {
         File[] files = file.listFiles();
         return files != null ? files : null;
      } else {
         return null;
      }
   }

   public static File[] getFileList(File directory) {
      File[] files = directory.listFiles();
      return files != null ? files : null;
   }

   public static long getFileSize(File file) {
      long size = 0L;
      if (!file.isDirectory()) {
         return file.length();
      } else {
         File[] files = file.listFiles();

         for(int i = 0; i < files.length; ++i) {
            if (files[i].isDirectory()) {
               size += getFileSize(files[i]);
            } else {
               size += files[i].length();
            }
         }

         return size;
      }
   }

   public void deleteFile(File f) {
      if (f.isDirectory()) {
         File[] files = f.listFiles();
         if (files != null && files.length > 0) {
            for(int i = 0; i < files.length; ++i) {
               this.deleteFile(files[i]);
            }
         }
      }

      f.delete();
   }

   public interface CopyListener {
      void startCopy();

      void progress(int var1);

      void finish(File var1);
   }
}
