package phototransfer.utils;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {
   private static final Pattern mFileNamePattern = Pattern.compile("([^<>/\\\\\\|:\"\"\\*\\?]+)\\.\\w+$");

   public static String getFilePath(String localFile) {
      String[] cols = localFile.trim().split(":");
      String fileName = cols[0];
      if (cols.length > 1) {
         fileName = cols[1];
      }

      Matcher matcher = mFileNamePattern.matcher(localFile);
      if (matcher.find()) {
         fileName = matcher.group(0);
      }

      String path = "D:/phototransfer";
      String parentPath = localFile.substring(0, localFile.indexOf(fileName));
      if (parentPath != null && !parentPath.isEmpty()) {
         path = path + "/" + parentPath;
      }

      File dir = new File(path);
      if (!dir.exists()) {
         dir.mkdirs();
      }

      File outFile = new File(path + "/" + fileName);
      if (outFile.exists()) {
         outFile.delete();
      }

      return path + "/" + fileName;
   }
}
