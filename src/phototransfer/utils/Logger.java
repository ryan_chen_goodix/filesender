package phototransfer.utils;

import android.util.Log;

public class Logger {
   private static boolean DEBUG = true;

   public static void e(String tag, String messag) {
      if (DEBUG) {
         Log.e(tag, messag);
      }

   }

   public static void d(String tag, String messag) {
      if (DEBUG) {
         Log.d(tag, messag);
      }

   }

   public static void w(String tag, String messag) {
      if (DEBUG) {
         Log.w(tag, messag);
      }

   }

   public static void v(String tag, String messag) {
      if (DEBUG) {
         Log.v(tag, messag);
      }

   }

   public static void i(String tag, String messag) {
      if (DEBUG) {
         Log.i(tag, messag);
      }

   }
}
