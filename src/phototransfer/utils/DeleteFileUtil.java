package phototransfer.utils;

import java.io.File;

public class DeleteFileUtil {
   public static boolean delete(String fileName) {
      File file = new File(fileName);
      if (!file.exists()) {
         System.out.println("删除文件失败：" + fileName + "文件不存在");
         return false;
      } else {
         return file.isFile() ? deleteFile(fileName) : deleteDirectory(fileName);
      }
   }

   public static boolean deleteFile(String fileName) {
      File file = new File(fileName);
      if (file.isFile() && file.exists()) {
         file.delete();
         System.out.println("删除单个文件" + fileName + "成功！");
         return true;
      } else {
         System.out.println("删除单个文件" + fileName + "失败！");
         return false;
      }
   }

   public static boolean deleteDirectory(String dir) {
      if (!dir.endsWith(File.separator)) {
         dir = dir + File.separator;
      }

      File dirFile = new File(dir);
      if (dirFile.exists() && dirFile.isDirectory()) {
         boolean flag = true;
         File[] files = dirFile.listFiles();

         for(int i = 0; i < files.length; ++i) {
            if (files[i].isFile()) {
               flag = deleteFile(files[i].getAbsolutePath());
               if (!flag) {
                  break;
               }
            } else {
               flag = deleteDirectory(files[i].getAbsolutePath());
               if (!flag) {
                  break;
               }
            }
         }

         if (!flag) {
            System.out.println("删除目录失败");
            return false;
         } else if (dirFile.delete()) {
            System.out.println("删除目录" + dir + "成功！");
            return true;
         } else {
            System.out.println("删除目录" + dir + "失败！");
            return false;
         }
      } else {
         System.out.println("删除目录失败" + dir + "目录不存在！");
         return false;
      }
   }

   public static void delFolder(String folderPath) {
      try {
         delAllFile(folderPath);
         String filePath = folderPath.toString();
         File myFilePath = new File(filePath);
         myFilePath.delete();
      } catch (Exception var3) {
         var3.printStackTrace();
      }

   }

   public static boolean delAllFile(String path) {
      boolean flag = false;
      File file = new File(path);
      if (!file.exists()) {
         return flag;
      } else if (!file.isDirectory()) {
         return flag;
      } else {
         String[] tempList = file.list();
         File temp = null;

         for(int i = 0; i < tempList.length; ++i) {
            if (path.endsWith(File.separator)) {
               temp = new File(path + tempList[i]);
            } else {
               temp = new File(path + File.separator + tempList[i]);
            }

            if (temp.isFile()) {
               temp.delete();
            }

            if (temp.isDirectory()) {
               delAllFile(path + "/" + tempList[i]);
               delFolder(path + "/" + tempList[i]);
               flag = true;
            }
         }

         return flag;
      }
   }
}
