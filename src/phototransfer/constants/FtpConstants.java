package phototransfer.constants;

public class FtpConstants {
   public static final String ROOT_DIR = "D:/phototransfer";
   public static final int FTP_PORT = 9000;
   public static final int BOARD_CAST_PORT = 8888;
   public static final String BOARD_CAST_IP_ADDRESS = "255.255.255.255";
   public static final long HEART_RATE_INTERVAL = 200L;
   public static final int SUCCESS = 0;
   public static final int FAILED = -1;
   public static final int SERVER_STATUS_ON_LINE = 4096;
   public static final int SERVER_STATUS_OFF_LINE = 4097;
}
