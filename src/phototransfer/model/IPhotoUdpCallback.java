package phototransfer.model;

public interface IPhotoUdpCallback {
   void onStartRecvFile(int var1, String var2);

   void onProgress(String var1, long var2, long var4);

   void onFinishedRecvFile(int var1, String var2);

   void onError(int var1, String var2);
}
