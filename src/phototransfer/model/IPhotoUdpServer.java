package phototransfer.model;

public interface IPhotoUdpServer {
   boolean startServer(int var1, IPhotoUdpCallback var2);

   void stopServer();
}
