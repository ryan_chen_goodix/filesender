package phototransfer.model.filetransfer;

import android.util.Log;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;

public class AckSender {
   public static final String TAG = "AckSender";
   private DatagramSocket socket = new DatagramSocket();
   private DatagramPacket packet;
   private ByteBuffer buffer = ByteBuffer.allocate(12);
   private int latestAck;
   private static final int ACK_SIZE = 12;

   public AckSender(InetAddress destination, int ackPort) throws SocketException {
      this.packet = new DatagramPacket(this.buffer.array(), 12, destination, ackPort);
      this.latestAck = 0;
   }

   public boolean sendAck(int ackNumber) {
      if (ackNumber > this.latestAck) {
         this.latestAck = ackNumber;
      }

      this.buffer.putInt(0, this.latestAck);
      this.buffer.putInt(4, this.latestAck);
      this.buffer.putInt(8, this.latestAck);

      try {
         this.socket.send(this.packet);
      } catch (IOException var3) {
         return false;
      }

      Log.d("AckSender", String.format("[send ack] %d", ackNumber));
      return true;
   }
}
