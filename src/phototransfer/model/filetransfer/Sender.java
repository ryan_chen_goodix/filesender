package phototransfer.model.filetransfer;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Sender {
   public static final String TAG = "Sender";
   private int sendingPort;
   private InetAddress destination;
   private File file;
   private AckReceiver ackReceiver;
   private FileSendBuffer sender;
   public static final int SEGMENT_SIZE = 1000;
   static final int EOF = -1;
   static final long ACK_TIMEOUT = 30000L;
   static final long THREAD_TIMEOUT = 1000L;

   public void sendFiles(String ipAddress, int port, List<String> fileNames) {
      Iterator var4 = fileNames.iterator();

      while(var4.hasNext()) {
         String name = (String)var4.next();
         this.sendFile(ipAddress, port, name);
      }

   }

   public void sendFile(String ipAddress, int port, String filePath) {
      long startTime = (new Date()).getTime();

      try {
         this.destination = InetAddress.getByName(ipAddress);
      } catch (UnknownHostException var54) {
         Log.e("Sender", "Invalid ip address", var54);
         return;
      }

      this.sendingPort = port;
      this.file = new File(filePath);
      BufferedInputStream reader = null;

      try {
         reader = new BufferedInputStream(new FileInputStream(this.file));
      } catch (FileNotFoundException var53) {
         Log.d("Sender", "[error] could not open the specified file. message: " + var53.getMessage());
         System.exit(1);
      }

      SocketException e;
      try {
         this.ackReceiver = new AckReceiver();
      } catch (SocketException var52) {
         e = var52;

         try {
            Log.d("Sender", "[error] could not create the ack receiver. message: " + e.getMessage());
            reader.close();
            this.sender.close();
         } catch (IOException var50) {
            Log.d("Sender", "[error] file reader failed to close: " + var50.getMessage());
         } finally {
            System.exit(1);
         }
      }

      try {
         this.sender = new FileSendBuffer(this.destination, this.sendingPort, this.ackReceiver);
      } catch (SocketException var49) {
         e = var49;

         try {
            Log.d("Sender", "[error] could not create a socket. message: " + e.getMessage());
            reader.close();
         } catch (IOException var47) {
            Log.d("Sender", "[error] file reader failed to close. message: " + var47.getMessage());
         } finally {
            System.exit(1);
         }
      }

      this.ackReceiver.setSendBuffer(this.sender);
      this.ackReceiver.start();
      this.sender.start();
      DataPacket initPacket = new DataPacket(this.file, this.ackReceiver.getPort());
      this.sender.sendPacket(initPacket);
      byte[] buffer = new byte[1000];
      int bytesRead = 0;

      try {
         bytesRead = reader.read(buffer);
      } catch (IOException var46) {
         Log.d("Sender", "[error] file read error. message: " + var46.getMessage());
         System.exit(1);
      }

      DataPacket lastPacket;
      while(bytesRead != -1) {
         lastPacket = new DataPacket(buffer, bytesRead, false);
         this.sender.sendPacket(lastPacket);

         try {
            bytesRead = reader.read(buffer);
         } catch (IOException var45) {
            Log.d("Sender", "[error] file read error. message: " + var45.getMessage());
            System.exit(1);
         }
      }

      lastPacket = new DataPacket((byte[])null, 0, true);
      this.sender.sendPacket(lastPacket);
      this.ackReceiver.waitForAck(lastPacket.getSequenceNumber(), 30000L);
      this.ackReceiver.stopListening();
      this.sender.stopSending();

      try {
         this.ackReceiver.join(1000L);
         this.sender.join(1000L);
      } catch (InterruptedException var44) {
         Log.d("Sender", "[error] interrupted while closing threads. transfer may not have finished normally.");
      }

      try {
         this.sender.close();
         reader.close();
      } catch (IOException var43) {
         Log.d("Sender", "[error] file reader failed to close: " + var43.getMessage());
      }

      System.out.println("[completed]");
      long runningTime = (new Date()).getTime() - startTime;
      long totalDataSent = this.sender.getTotalDataSent();
      long fileSize = this.file.length();
      System.out.format("[stats] running time: %d ms\n", runningTime);
      System.out.format("[stats] file size: %d bytes\n", fileSize);
      System.out.format("[stats] total data sent: %d bytes\n", totalDataSent);
      double efficiency = 0.0D;
      if (totalDataSent > 0L) {
         efficiency = (double)fileSize / (double)totalDataSent;
      }

      System.out.format("[stats] efficiency: %04.2f percent\n", efficiency * 100.0D);
   }
}
