package phototransfer.activity;

import android.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import phototransfer.model.IPhotoUdpCallback;
import phototransfer.model.IPhotoUdpServer;
import phototransfer.model.filetransfer.Sender;
import phototransfer.model.udp.BroadcastReceiver;
import phototransfer.model.udp.UdpFileReceiver;

public class Application {
   public static final String TAG = "Application";
   IPhotoUdpCallback mIPhotoUdpCallback = new IPhotoUdpCallback() {
      public void onStartRecvFile(int status, String fileName) {
         Log.d("Application", "onStartRecvFile fileName :" + (new File(fileName)).getName());
      }

      public void onProgress(String fileName, long total, long current) {
         int progress = (int)(current * 100L / total);
         if (progress % 2 == 0) {
            Log.d("Application", "onProgress <" + fileName + ">  received percent :" + current * 100L / total + "%");
         }

      }

      public void onFinishedRecvFile(int status, String fileName) {
         Log.d("Application", "onFinishedRecvFile\n\n");
      }

      public void onError(int status, String message) {
         if (null != message) {
            Log.e("Application", "message :" + message);
         }

      }
   };

   public void startSender(String filePath) {
      Log.d("Application", "file path: " + filePath);
      if (null != filePath && !filePath.isEmpty()) {
         BroadcastReceiver.getInstance().startRecvHeartBeat(8888);

         try {
            Thread.sleep(1000L);
         } catch (InterruptedException var10) {
            var10.printStackTrace();
         }

         String[] tmp = BroadcastReceiver.getInstance().stopRecvHeartBeat();
         String[] addressArr = new String[tmp.length + 1];

         for(int i = 0; i < tmp.length; ++i) {
            addressArr[i] = tmp[i];
         }

         File dir = new File(filePath);
         List<String> fileNames = new ArrayList();
         if (null == dir.list()) {
            Log.e("Application", "dir is empty!");
         } else {
            File[] var6 = dir.listFiles();
            int var7 = var6.length;

            int var8;
            for(var8 = 0; var8 < var7; ++var8) {
               File f = var6[var8];
               if (f.isFile()) {
                  fileNames.add(f.getAbsolutePath());
               }
            }

            String[] var12 = addressArr;
            var7 = addressArr.length;

            for(var8 = 0; var8 < var7; ++var8) {
               String address = var12[var8];
               (new Sender()).sendFiles(address, 9000, fileNames);
            }

            Log.d("Application", "Sender exit");
         }
      } else {
         Log.d("Application", "Invalid file path");
      }
   }

   public void startReceiver() {
      IPhotoUdpServer photoUdpServer = new UdpFileReceiver();
      photoUdpServer.startServer(9000, this.mIPhotoUdpCallback);
   }

   private boolean parseArgs(String[] args) {
      int argc = args.length;
      if (argc < 1) {
         Log.d("Application", "As a client usage: sendfile -f <filePath>");
         Log.d("Application", "As a server usage: sendfile -s");
         return false;
      } else {
         int i = 0;
         if (args[i].equals("-f") && i + 1 < argc) {
            String filePath = args[i + 1];
            this.startSender(filePath);
         } else if (args[i].equals("-s")) {
            this.startReceiver();
         } else {
            Log.e("Application", "invalid param --> " + args[0]);
         }

         return true;
      }
   }

   public static void main(String[] args) {
      (new Application()).parseArgs(args);
   }
}
